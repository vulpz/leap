'use strict';

const Express = require('express');
const Request = require('request');
const Parse = require('parse/node');
const BodyParser = require('body-parser');
const CookieParser = require('cookie-parser');
const Session = require('express-session');

const app = new Express();

// Set to use parsers and session modules.
app
	.use(Express.static('public'))
	.use(CookieParser())
	.use(BodyParser.urlencoded({
		extended: false,
		strict: false
	}))
	.use(BodyParser.json())
	.use(Session({
		secret: 'secret1337Token!',
		resave: true,
		saveUninitialized: false
	}));

// Set to use ejs templates.
app
	.set('view engine', 'ejs')
	.set('views', (__dirname + '/views'));

// Use middleware.
// - check for sess;
app.use((req, res, next) => {
	if((req.url == '/viewData') && (!req.session.token)) {
		res.render('index', {
			message: 'Bad session. Please login again.'
		});
	} else {
		next();
	}
});

// Home; forces login page if no sess.
app.get('/', (req, res) => {
	res.render('index', {
		message: ''
	});
});

// POST login form and attempt login via Leap's endpoint.
app.post('/doLogin', (req, res) => {
	let postData = {
		'username': req.body.username,
		'password': req.body.password,
		'source': req.body.source
	};
	connectToLeap(req, postData)
		.then((session) => {
			queryParse(session)
				.then((dbData) => {
					res.render('viewData', {
						queryLength: dbData.length,
						queryData: dbData
					});
				})
				.catch((error) => {
					console.log(error);
				});
		})
		.catch((error) => {
			console.log(error);
			res.send('Whoops, something went wrong!');
		});
});

// Data view page; auto redirect from successful login.
//	if requested directly, checks for token; redirect to login if none.
app.get('/viewData', (req, res) => {
	queryParse(req.session.token)
		.then((dbData) => {
			res.render('viewData', {
				queryLength: dbData.length,
				queryData: dbData
			});
		})
		.catch((error) => {
			console.log(error);
		});
});

// Spin up server!
app.listen(80, 'localhost');


/*-----------------------------------*/


// Connects to Leap login endpoint;
// 	returns a promise with session token to begin query.
let connectToLeap = (req, postData) => {
	console.log('Connection being established..');
	return new Promise((resolve, reject) => {
		Request({
			url: 'https://apitest.leaptodigital.com/parse/functions/loginUser',
			method: 'POST',
			json: true,
			headers: {
				'X-Parse-Application-Id': '123456',
				'X-Parse-Session-Token': ''
			},
			body: postData
		}, (e, response, body) => {
			if(response.statusCode == 200) {
				// Set session cookie to hold app token.
				let sess = body.result.result.sessionToken;
				req.session.token = sess;
				resolve(sess);
			} else {
				reject('What we have here, is a failure, to communicate.');
			}
		});
	});
}

let queryParse = (sess) => {
	console.log('Parse query initialized..');
	return new Promise((resolve, reject) => {
		Parse.initialize('123456');
		Parse.serverURL = 'https://apitest.leaptodigital.com/parse';

		let SSMeasureSheetItem = Parse.Object.extend('SSMeasureSheetItem');
		
		let query = new Parse.Query(SSMeasureSheetItem);
		query.find({sessionToken: sess})
			.then(data => {
				// On successful query, send data.
				resolve(data);
			})
			.catch(() => {
				reject('bah! Query issue; go check the parse block.');
			});
	});
}
